###### 玩转金刚梯>金刚字典>
### 金刚号梯的配置
- [苹果手机](/LadderFree/Apple/iPhone/KKLadderKKID/KKLadderKKIDConfigure.md)
- [苹果平板](/LadderFree/Apple/iPad/KKLadderKKID/KKLadderKKIDConfigure.md)
- [苹果电脑](/LadderFree/Apple/MacOS/KKLadderKKID/KKLadderKKIDConfigure.md)
- [安卓类手机](/LadderFree/Android/Phone/KKLadderKKID/KKLadderKKIDConfigure.md)
- [安卓类平板](/LadderFree/Android/Pad/KKLadderKKID/KKLadderKKIDConfigure.md)
- [安卓类机顶盒](/LadderFree/Android/TVBox/KKLadderKKID/KKLadderKKIDConfigure.md)
- [Windows](/LadderFree/Windows/WinAllVersion/KKLadderAPP/KKLadderAPPConfigure.md)

#### 返回到
- [玩转金刚梯](/LadderFree/A.md)
- [金刚字典](/LadderFree/kkDictionary/KKDictionary.md)



