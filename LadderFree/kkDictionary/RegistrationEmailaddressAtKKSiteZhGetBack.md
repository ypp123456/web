###### 玩转金刚梯>金刚字典>

### 如何找回在金刚网的注册邮箱？

- 如果您记得您的[ 金刚号 ](/LadderFree/kkDictionary/KKID.md)，请[ 电邮客服 ](mailto:cs@a2zitpro.com)， 告诉客服您的[ 金刚号 ](/LadderFree/kkDictionary/KKID.md)是什么，客服也许能帮您找回您的<strong> 注册邮箱 </strong >
- 在您常用的邮箱里找找，看看是否有[ 金刚网 ](/LadderFree/kkDictionary/KKSiteZh.md)之前发给您的邮件。如果有，则应尝试用该邮箱登入[ 金刚网 ](/LadderFree/kkDictionary/KKSiteZh.md)
- [ 重新注册 ](/l2_reg.md)[ 金刚账户 ](/LadderFree/kkDictionary/KKAccount.md)


#### 返回到
- [玩转金刚梯](/LadderFree/A.md)
- [金刚字典](/LadderFree/kkDictionary/KKDictionary.md)

